
from niño import Niño
from planta import Planta
from hierba import Hierba
from trebol import Trebol 
from rabano import Rabano 
from violeta import Violeta
import random
class Garden():

    #Definimos los parámetros
    def __init__(self):
        #Declaramos los atributos (privados ocultos)
        self._nombre = "Escuelita bioinformatica"
        self._niños_lista = [Niño("Alicia"),Niño("Andrés"),Niño("Belen"),Niño("David"),Niño("Eva")
                            ,Niño("José"),Niño("Larry"),Niño("Lucia"),Niño("Marit"),Niño("Pepito")
                            ,Niño("Rocío"),Niño("Sergio")]
        
        
        
        


    @property
    #Definimos el método para obtener el nombre
    def nombre(self):
        #estamos retornando el atributo privado oculto
        return self._nombre
    @property
    
    def niños_lista(self):
        return self._niños_lista

    #funcion que asigna plantas aleatoreamente 
    def asignar_planta(self):

        for x in self._niños_lista:
            for i in range(4):
                azar = random.randint(0,3)
                if azar == 0:
                    x.plantas = Hierba() 
                elif azar == 1:
                    x.plantas = Trebol()
                elif azar == 2:
                    x.plantas = Rabano()
                else:
                    x.plantas = Violeta()

    def ventana_alumno_seleccionado(self, persona):
        texto = ""
        texto2 = ""
        if isinstance (persona,Niño):
            #recorre los niños en la lista de objetos tipo Niño
            for kid in self._niños_lista:
                if kid == persona:# se compara si el objeto Niño es igual al Seleccionado 
                    plantitas = kid.plantas# se guarda una variable, las plantas del niño
                    for item in range(4):#for que recorrera 4 veces a contar desde el 0 sin contar el 4
                        if item == 0 or item == 1:#aqui se compara para agregar solamente las 2 primeras plantas
                            texto += plantitas[item].nombre#texto = variable que se le asignaran las 2 primeras plantas
                        elif item == 2 or item == 3:#aqui se compara  el item  con una constante para  saber el valor de item
                            texto2 += plantitas[item].nombre#aqui se le asigna a texto2 la 3era y 4ta planta del niño
                if kid != self._niños_lista[-1]:#imprime guiones y lo limita a no imprimir despues del ultimo
                    texto += " - - "
                if kid != self._niños_lista[-1]:
                    texto2 += " - - "
        print(texto)
        print(texto2)
      #  print(self._niños_lista[0].nombre)   