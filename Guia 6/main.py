from garden import Garden
#funcion que crea la ventana que contiene todas las plantas
def ventana(jardin):
  texto = ""
  texto2 = ""
  for niño in jardin.niños_lista:#for que recorre la lista niños
    plantitas = niño.plantas#aqui se asigna una variable para guardar la planta de los niño
    for item in range(4):#for que recorrera 4 veces a contar desde el 0 sin contar el 4
      if item == 0 or item == 1:#aqui se compara para agregar solamente las 2 primeras plantas
        texto += plantitas[item].nombre#texto = variable que se le asignaran las 2 primeras plantas
      elif item == 2 or item == 3:#aqui se compara  el item  con una constante para  saber el valor de item
        texto2 += plantitas[item].nombre#aqui se le asigna a texto2 la 3era y 4ta planta del niño
    if niño != jardin.niños_lista[-1]:#imprime guiones y lo limita a no imprimir despues del ultimo
      texto += " - "
    if niño != jardin.niños_lista[-1]:
      texto2 += " - "
  print("[ventana]               [ventana]               [ventana]")
  print(texto)
  print(texto2)

def main():

  jardin = Garden()

  jardin.asignar_planta()
  while True:
    print("bienvenido")
    print("<1>,quiere conocer la lista de alumnos")
    print("<2>,¿quiere ver el orden de las plantas en la ventana ?")
    print("<3>,quiere conocer la plantas de un alumno en especifico")
    print("<4>,quiere conocer las plantas de todos los alumnos")
    print("<5>,salir del programa")
    opcion = int(input("ingrese su opcion: "))
    if opcion == 1 :
      curso = ""#variable de tipo str
      for niños in jardin.niños_lista:#recorre la lista de niños
        curso += niños.nombre#se suma el nombre al str curso
        curso += " - "#al mismo tiempo de agregado un nombre se le agrega un guion
      print(curso)
      
    elif opcion == 2:
      ventana(jardin)# se imprime la ventana jardin

    elif opcion == 3:
      i = 1#variable 
      for niños in jardin.niños_lista:#recorre la lista de niños
        print(f"{i}-{niños.nombre}")#print con f string para asignar valores y que vallan cambiando 
        i += 1#despues de cada recorrido que sume 1
      opcion2 =int(input("ingrese su opcion: "))#luego de tener la lista que vuelva a seleccionar un alumno 
      #funcion ya descrita anteriorimente que recorre la lista y se le resta 1 para coincidir con la 
      #opcion pedida
      jardin.ventana_alumno_seleccionado(jardin.niños_lista[opcion2-1])
    elif opcion == 4:
      
      for niño in jardin.niños_lista:#for que recorre la lista de niños 
        plantitas = niño.plantas#se le asigna a una variable las plantas del niño
        texto = ""
        for item in range(4):
          texto += plantitas[item].nombre#aqui se recorre la totalidad de plantas por niño
          if item != 3:#variable de control para que no imprima un guion despues del ultimo parametro
            texto +=" - "
        print(f"{niño.nombre}: {texto}")#print que imprime el nombre y las plantas de cada niño

    elif opcion == 5:
      print("la simulacion a terminado")
      break
    else:
      print("*************************************************")
      print("opcion ingresada no es valida, intentelo de nuevo")
      print("*************************************************")
    
  
  



if __name__ == '__main__':
    main()