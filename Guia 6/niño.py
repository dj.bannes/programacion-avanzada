from planta import Planta

class Niño():

    #Definimos los parámetros
    def __init__(self, nombre):
        #Declaramos los atributos (privados ocultos)
        self._nombre = nombre
        self._plantas = []


    @property
    #Definimos el método para obtener el nombre
    def nombre(self):
        #estamos retornando el atributo privado oculto
        return self._nombre
    
    @nombre.setter
    #Definimos el metodo para entregar o dar el nombre en el atributo privado
    def nombre(self, nombre):
        # Condición si el parametro es de Tipo String
        if isinstance(nombre, str):
            self._nombre = nombre
        else:
            print("El tipo de dato no corresponde")
    @property

    def plantas(self):
        return self._plantas
    
    @plantas.setter
    
    def plantas(self,plantas):
        if isinstance(plantas,Planta):
            self._plantas.append(plantas)
        else:
            print("El tipo de dato no corresponde")
    
    