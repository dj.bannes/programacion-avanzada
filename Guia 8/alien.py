import arma as a

# Se define la clase Alien
class Alien:
    # Constructor
    def __init__(self,x,y):
        self._x = x
        self._y = y
        self._salud = 3
        self._is_alive = True
        self._arma = None

    
    @property
    def cordenada_x(self):
        return self._x

    @property
    def cordenada_y(self):
        return self._y

    @property
    def salud(self):
        return self._salud
    
    @property
    def armado(self):
        return self._arma
    @armado.setter
    def armado(self, arma):
        if isinstance(arma, a.Arma):
            self._arma = arma
        else:
            print("No es de tipo Arma")


    # Se define el metodo hit
    def hit(self):
        self._salud = self._salud - 1


    # Se define el metodo que revisa si el alien esta vivo
    def is_alive(self):
        if self._salud <= 0:
            self._is_alive = False
        return self._is_alive


    # Se define el metodo que teletransporta al alien
    def teleport(self,x,y):
        self._x = x
        self._y = y
    
    
    
    

