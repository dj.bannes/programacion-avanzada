# Se define la clase Arma que sera una clase padre
class Arma:
    # Constructor
    def __init__(self):
        self._nombre = None
        self._damage = 0

    @property
    def nombre(self):
        return self._nombre
    
    @property
    def damage(self):
        return self._damage
