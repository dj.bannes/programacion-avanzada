from arma import Arma

# Clase Canon_Protones hija de Arma
class Canon_Protones(Arma):
    # Constructor
    def __init__(self):
        super().__init__()
        self._nombre = "Cañon de Protones"
        self._damage = 2
