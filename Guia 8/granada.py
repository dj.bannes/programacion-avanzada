from arma import Arma

# Clase Granada_Antimateria hija de Arma
class Granada_Antimateria(Arma):
    # Constructor
    def __init__(self):
        super().__init__()
        self._nombre = "Granada Antimateria"
        self._damage = 3