from alien import Alien 
from arma import Arma
from granada import Granada_Antimateria
from pistola import Pistola_Laser
from canon import Canon_Protones
import random
import time


# Se define la clase Juego
class Juego: 
    # Constructor
    def __init__ (self):
        self._alien_creados = []
        self._size = random.randint(5, 9)
        
        # Se crea la matriz del juego
        matriz = []
        for x in range(self._size):
            tmp = []
            for y in range(self._size):
                tmp.append(None)
            matriz.append(tmp)

        self._matrix = matriz
    
    @property
    def size(self):
        return self._size

    @property
    def aliens(self):
        return self._alien_creados
    

    # Este metodo imprime la matriz de juego
    def imprime_matriz(self):
        print("\n")
        text = "  "
        for x in range(self.size):
            text += f"{x+1} "
        print(text)
        for fil in self._matrix:
            fila = f"{self._matrix.index(fil)+1}|"
            for col in fil:
                if not col:
                    fila += " |"
                else:
                    if isinstance(col, Alien):
                        fila += "A" + "|"
            print(fila)
        print("\n")


    # Este metodo crea un alien a partir de coordenadas x e y
    def crear_alien(self, x, y): 
        
        alien = Alien(x,y)
        self._alien_creados.append(alien)
        self._matrix[x-1][y-1] = alien
        self.imprime_matriz()

        aux = random.randint(1,4)
        if aux == 1:
            alien.armado = Pistola_Laser()
        elif aux == 2:
            alien.armado = Canon_Protones()
        elif aux == 3:
            alien.armado = Granada_Antimateria()

    # Este metodo retorna la cantidad de aliens creados
    def total_aliens_creados(self):
        return len(self._alien_creados)
    

    # Este metodo se encarga de la colision entre 2 objetos
    def colision(self, obj1, obj2):
        if obj1._x == obj2._x and obj1._y == obj2._y:
            if isinstance(obj1, Alien):
                obj1.hit()
            if isinstance(obj2, Alien):
                obj2.hit()
    

    # Este metodo se encarga de disparar las armas de los aliens
    def disparar(self, alien):
        if isinstance(alien, Alien):
            # Si el alien posee un arma, entonces puede disparar
            if isinstance(alien.armado, Arma):
                diana = (random.randint(0, self.size), random.randint(0, self.size))
                # Si el disparo alcanza algun alien, este pierde vida
                if alien.cordenada_x == diana[0] and alien.cordenada_y == diana[1]:
                    # Dependiendo del tipo de arma, el alien toma mas o menos daño
                    for num in range(alien.armado.damage):
                        alien.hit()
                    # Despues de cada disparo se revisa que aliens siguen vivos
                    for item in self.aliens:
                        if item.cordenada_x == diana[0] and item.cordenada_y == diana[1]:
                            if not item.is_alive():
                                print("El alien murio")
                            else:
                                print(f"Al alien le quedan {item.salud} puntos de vida")
                    print(f"Le hizo {alien.armado.damage} de daño a un alien en ({diana[0]},{diana[1]})")
                # El alien falla el disparo
                else:
                    print("Fallo el disparo")
            # El alien no puede disparar si no tiene arma
            else:
                print("Este alien no tiene arma")
    

    # Este metodo simula una partida de 3 rondas
    def ciclo_de_juego(self):
        for x in range(3):
            for alien in self.aliens:
                # Hay una probabilidad de que el alien se teletransporte
                y = random.randint(1,5)
                if y == 1:
                    print("\nUn alien se teletransporta...")
                    new_x = random.randint(1, self.size)
                    new_y = random.randint(1, self.size)
                    old_x = alien.cordenada_x
                    old_y = alien.cordenada_y
                    alien.teleport(new_x, new_y)
                    # Este ciclo revisa si es que hay colision entre 2 aliens en la misma casilla
                    for item in self.aliens:
                        if (item.cordenada_x == new_x and item.cordenada_y == new_y) and item != alien:
                            self.colision(item, alien)
                            if not alien.is_alive():
                                print("El alien murio")
                            else:
                                print(f"Al alien le quedan {item.salud} puntos de vida")
                            if not item.is_alive():
                                print("El otro alien murio")
                            else:
                                print(f"Al otro alien le quedan {item.salud} puntos de vida")
                    self._matrix[old_x-1][old_y-1] = None
                    self._matrix[new_x-1][new_y-1] = alien
                    self.imprime_matriz()
 
                # El alien dispara
                print("\nUn alien va a disparar...")
                time.sleep(1)
                self.disparar(alien)
                self.imprime_matriz()
                time.sleep(1)
 