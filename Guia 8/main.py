#integrantes
#Diego Nuñez, Sebastian Benavides

from juego import Juego
import random
import time


# Se define la funcion pedida para que cree aliens en posiciones dadas
def new_aliens(juego, lista):
    for coord in lista:
        juego.crear_alien(coord[0], coord[1])
        time.sleep(1)


# Funcion main 
def main():

    # Se crea un objeto tipo Juego
    juego = Juego()

    lista_inicial = [(1,1), (4,3)]

    print("\nSe crean los aliens...")
    new_aliens(juego, lista_inicial)

    aliens_a_crear = random.randint(3, 5)
    for y in range(aliens_a_crear):
        juego.crear_alien(random.randint(0,juego.size), random.randint(0,juego.size))
        time.sleep(1)

    # Este metodo es el corazon del programa
    juego.ciclo_de_juego()
    

if __name__ == "__main__":
    main()
