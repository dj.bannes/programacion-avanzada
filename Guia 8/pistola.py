from arma import Arma

# Clase Pistola_laser hija de Arma
class Pistola_Laser(Arma):
    # Constructor
    def __init__(self):
        super().__init__()
        self._nombre = "Pistola Laser"
        self._damage = 1