
from figther import Figther
import random

class Wizard(Figther):
    # Constructor
    def __init__(self):
        self._nombre = "Mago"
        self._preparacion_hechizo = None
        self._hechizo_damage = None
        self._vulnerable = None

    def To_string(self):
        print(self._nombre)

    @property
    def is_Vulnerable(self):
        return self._nombre

    @is_Vulnerable.setter 
    def is_Vulnerable(self,nombre):
        if isinstance (nombre, Figther):
            self._vulnerable = False
        return self._vulnerable

    def prepareSpell(self):
        preparacion = random.randint(0,1)#prepara hechizo
    
        if preparacion == 0:
            self._preparacion_hechizo  = False
            self._vulnerable = True
        else:
            self._preparacion_hechizo = True

        return self._preparacion_hechizo

    def damage(self):
        if self._preparacion_hechizo == True:
            self._hechizo_damage = 12
        else:
            self._hechizo_damage = 3
            
        return self._hechizo_damage