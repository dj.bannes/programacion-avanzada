
class Ciudadano ():
    def __init__(self,_id):
        self.__comunidad = None
        #Para identificar a la comunidad que pertenece el ciudadano 
        self.__id = _id
        #la identificacion del ciudadano 
        self.__amigos = []#grupo de amigos(contactos posibles) 
        self.__estado = True 
        # si es verdadero, indica que el ciudadano esta sano o muerto
        self.__contador = 0 # contador para pasar a estar recuperado 

    #get__comunidad
    def get__comunidad(self):
        return self.__comunidad
    #set__comunidad
    def set__comunidad(self,a):
        self.__comunidad = a

    #get__id
    def get__id(self):
        return self.__id
    #set__id
    def set__id(self,b):
        self.__id = b

    #get__amigos
    def get__amigos(self):
        return self.__amigos
    #set__amigos
    def set__amigos(self,c):
        self.__amigos.append(c)
    #get__estado
    def get__estado(self):
        return self.__estado
    #set__estado
    def set__estado(self,d):
        self.__estado = d
    #get__contador
    def get__contador(self):
        return self.__contador
    #set__contador
    def set__contador(self,e):
        self.__contador = e


