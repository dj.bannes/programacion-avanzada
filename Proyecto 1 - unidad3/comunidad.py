from ciudadano import Ciudadano
import random

class Comunidad():
    
    def __init__(self,nombre_comunidad,numero_ciudadanos_ciudad,
                 Numero_de_amigos, probabilidad_conexion_fisica):
        self.__nombre_comunidad = nombre_comunidad
        self.__num_ciudadanos = numero_ciudadanos_ciudad
        # el numero de ciudadanos en la comnunidad
        self.__probabilidad_conexion_fisica = probabilidad_conexion_fisica
        #probabilidad que haya conexion fisica
        self.__Numero_de_amigos = Numero_de_amigos # cantidad de amigos
        self.__Lista_ciudad_sana =[] 
        self.__Lista_ciudad_enferma = []
        self.__Lista_ciudad_recuperada = []

    #get__nombre_comunidad
    def get__nombre_comunidad(self):
        return self.__nombre_comunidad
    #set__nombre_comunidad
    def set__nombre_comunidad(self,a):
        self.__nombre_comunidad = a

    #get__num_ciudadanos
    def get__num_ciudadanos(self):
        return self.__num_ciudadanos
    #set__num_ciudadanos
    def set__num_ciudadanos(self,b):
        self.__num_ciudadanos = b
    
    #get__Numero_de_amigos
    def get__Numero_de_amigos(self):
        return self.__Numero_de_amigos
    #set__Numero_de_amigos
    def set__Numero_de_amigos(self,c):
        self.__Numero_de_amigos = c
    #get__Lista_ciudad_sana
    def get__Lista_ciudad_sana(self):
        return self.__Lista_ciudad_sana
    #set__Lista_ciudad_sana
    def set__Lista_ciudad_sana(self,d):
        self.__Lista_ciudad_sana.append(d)
    #get__Lista_ciudad_enferma
    def get__Lista_ciudad_enferma(self):
        return self.__Lista_ciudad_enferma
    #set__Lista_ciudad_enferma
    def set__Lista_ciudad_enferma(self,e):
        self.__Lista_ciudad_enferma.append(e)
    
    #get__probabilidad_conexion_fisica
    def get__probabilidad_conexion_fisica(self):
        return self.__probabilidad_conexion_fisica
    #set__probabilidad_conexion_fisica
    def set__probabilidad_conexion_fisica(self,f):
        self.__probabilidad_conexion_fisica = f
    #get__Lista_ciudad_recuperada
    def get__Lista_ciudad_recuperada(self):
        return self.__Lista_ciudad_recuperada
    #set__Lista_ciudad_recuperada
    def set__Lista_ciudad_recuperada(self,g):
        self.__Lista_ciudad_recuperada.append(g)
    
    def crear_ciudadanos(self):
        i = 0
        entregar_id = 0 
        while i < self.__num_ciudadanos:

            ciudadanoX = Ciudadano(entregar_id)
            ciudadanoX.set__comunidad(self.get__nombre_comunidad())
            #se entrega el nombre de la comunidad al ciudano
            self.set__Lista_ciudad_sana(ciudadanoX)
            #se envia el ciudadado a la lista de ciudad sana 
            i += 1  
            entregar_id += 1#id del ciudadano
 
    def lista_de_amigos(self):
        n = self.__Numero_de_amigos
        #variable corta
        s = len(self.__Lista_ciudad_sana)
        #largo de la lista de ciudadanos 
        for i in self.__Lista_ciudad_sana:
            # recorre la lista de personas sanas 
            j = random.randint( 0, s - n )  
            # numero aleatorio, define el inicio del id de los amigos
            for l in range(0,n):
                vk = j+l 
                # numero aleatorio + el rango entre 0  y n-1
                if vk == i.get__id(): 
                    #si no es == se agrega con normalidad vk
                    # si el rango entre 0 y n-1  es igual al id ingresa al if
                    if vk <= (s-n)-1 :
                    # s-n = ( la lista de ciudadanos - la cantidad de amigos)
                    # (-1 no salirse)
                    #si ingresa se le suma n
                        vk += n
                    elif vk >= n:
                    # ingresa solo cuando es mayor que la condicion anterior
                        vk -= n
                        #si ingresa se le resta n 
                    else : #control para casos extremos(casos limitados)
                        if j+n-1 < s-1:
                            '''cuando la cantidad de amigos es muy cercana 
                            a la cantidad  de ciudadanos totales, entonces el
                            id agregado sera el ultimo o el primero '''
                            vk = s-1
                        else:
                            vk = 0

                i.set__amigos(vk)#se agrega el amigo

    def impresion_general(self):
       
        print("susceptibles: ", len(self.__Lista_ciudad_sana),
                "total de contagios son: ",
                (self.__num_ciudadanos) - (len(self.__Lista_ciudad_sana)),
                "total de recuperados: ",len(self.__Lista_ciudad_recuperada),
                "casos activos: ",(len(self.__Lista_ciudad_enferma)))


        
           
           

        
        
