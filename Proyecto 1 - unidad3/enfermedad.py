
class Enfermedad():
    def __init__(self,comunidad,primer_infectado,infeccion_probable,promedio_pasos):
        comunidadxX = comunidad
        #la comunidad misma(desde el main)
        self.__primer_infectado = primer_infectado
        # ingresa a partir del random en el main
        self.__infeccion_probable = infeccion_probable
        # la probabilidad de que un ciudadano sano, sea infectado     
        self.__promedio_pasos = promedio_pasos 
        #tiempo promedio para ser declarado de "alta" o muerto
        
  
        comunidadxX.get__Lista_ciudad_sana()[self.__primer_infectado].set__estado(False)
        #se infecta a la primera persona
        comunidadxX.get__Lista_ciudad_sana()[self.__primer_infectado].set__contador(1)
        #desde que se infecta se considera el primer dia
        comunidadxX.set__Lista_ciudad_enferma(comunidadxX.get__Lista_ciudad_sana()[self.__primer_infectado])
        #se le entrega el infectado o a la lista correspondiente 
        comunidadxX.get__Lista_ciudad_sana().pop(self.__primer_infectado)
        #.pop elimina el elemento, ya que es el primer infectado, no hay problema 
        #.remove elimina el objeto no importa donde se encuentre 

    #get__primer_infectado
    def get__primer_infectado(self):
        return self.__primer_infectado   
    #set__primer_infectado
    def set__primer_infectado(self,a): 
        self.__primer_infectado = a
    #get__infeccion_probable
    def get__infeccion_probable(self):
        return self.__infeccion_probable  
    #set__infeccion_probable
    def set__infeccion_probable(self,b):
        self.__infeccion_probable = b
    #get__contador
    def get__promedio_pasos(self):
        return self.__promedio_pasos
    #set__contador
    def set__promedio_pasos(self,c):
        self.__promedio_pasos = c
