from random import random
# libreria random  que entrega numeros al azar desde 0 a 1


class Simulador():
    def __init__(self,comunidad):#constructor
               
        self.CiudadX = comunidad
        #comunidad entregada desde el main 
    
        self.CiudadX.crear_ciudadanos()
        #se crean los ciudadanos
        self.CiudadX.lista_de_amigos()
        #crea la lista de amigos

    '''buscar_id: toma un identificador de cualquier ciudadano(enviado desde la
    funcion buscar amigosI)
    en busqueda del mismo para poder obtener el objeto de tipo ciudadano
    para luego ser retornado a la funcion grupo de amigos'''
    def buscar_id(self,id_buscado):
        son_iguales_s = False 
        son_iguales_e = False 
        for i in self.CiudadX.get__Lista_ciudad_sana():
            if i.get__id() == id_buscado:
                son_iguales_s = True
                son_iguales_e = True
                return i
        if son_iguales_s == False:
            for j in self.CiudadX.get__Lista_ciudad_enferma():
                if j.get__id() == id_buscado:
                    son_iguales_s = True
                    son_iguales_e = True
                    return j 
        if son_iguales_e == False:
            for k in self.CiudadX.get__Lista_ciudad_recuperada():
                if k.get__id() == id_buscado:
                    
                    return k   

    def grupo_amigosI(self, primer_infectado,enfermedad):
        #funcion grupo amigos del infectado 
        grupo_de_amigosI = primer_infectado.get__amigos()
        #se cambia la variable 
        
        for i in grupo_de_amigosI:
            #for que recorre el grupo de amigos
            #se considera que se juntaron 

            if random() < self.CiudadX.get__probabilidad_conexion_fisica():
                '''random que  lanza numeros desimales aleatorios
                entre 0 y 1/// get__probabilidad conexion fisica se entrega
                desde el main, si ingresa es porque si hubo conexion fisica 
                entre las personas '''
                if random() < enfermedad.get__infeccion_probable():
                    '''get__infeccion problable se ingresa desde el main
                    y si ingresa aqui se considera infectado el ciudadano'''
                    persona = self.buscar_id(i)
                    #como el i es solo un numero que representa el id 
                    #se ingresa a la funcion buscar id que obtendra de 
                    # vuelta un objeto
                    persona.set__estado(False)
                    #se le cambia el estado a la persona
                

    def pasos(self,pasos_totales,enfermedad):
      #  self.grupo_amigosI(primer_infectado) 
        
        for i in range(0,pasos_totales):
            print("dia :", i+1 )
            for j in self.CiudadX.get__Lista_ciudad_enferma():
                self.grupo_amigosI(j,enfermedad)
            Lista_temporal_enfermos=[]#se actualiza por cada enfermo
            for k in self.CiudadX.get__Lista_ciudad_sana():
                #recorre la ciudad sana 
                if k.get__estado() == False:
                    #busca a la gente enferma
                    Lista_temporal_enfermos.append(k)
                    #agrega la gente a la lista temporal 
                    self.CiudadX.set__Lista_ciudad_enferma(k)
                    # agrega a la gente enferma a la lista de  enfermos
                    
            for l in Lista_temporal_enfermos:
                #recorre la lista temporal para que no haya 
                # conflicto en el for anterior
                self.CiudadX.get__Lista_ciudad_sana().remove(l)
                #se remueve de la lista de sanos a travez del objeto en 
                #la lista temporal
            Lista_temporal_recuperados=[]#se actualiza por cada recuperado
            for m in self.CiudadX.get__Lista_ciudad_enferma():
                '''for para contador  que recorre la lista de enfermos'''
                m.set__contador(m.get__contador()+1)
                #se le entrega el mismo valor que ya tenia y se le suma 1
                if m.get__contador() >= enfermedad.get__promedio_pasos():
                    #se recupera en el dia  >= al promedio de pasos
                    Lista_temporal_recuperados.append(m)
                    #se agrega el ciudadano a la lista temporar de recuperados 
                    self.CiudadX.set__Lista_ciudad_recuperada(m)
                    #append que agrega m a la lista de recuperados
            for n in Lista_temporal_recuperados:
                #for para recorrer la lista temporal y obtener al objeto 
                #recuperado
                self.CiudadX.get__Lista_ciudad_enferma().remove(n)
                #luego de obtenerlo lo remueve de la lista de enfermos 
            self.CiudadX.impresion_general()#funcion impresora
