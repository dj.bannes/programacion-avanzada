# Proyecto 2 Unidad 3 - Programacion Avanzada
La finalidad de este programa es dar solución a partir del proyecto 1 de la unidad 3  de programación avanzada, la que trata sobre la propagación de una enfermedad infecciosa en una comunidad. En esta segunda parte hay que aplicar vacunas para el control de la enfermedad infecciosa, como también, darle una  afección o una enfermedad base que complica la salud del ciudadano. Cada una de estas nuevas atribuciones tendrán un porcentaje que los haría decidir entre la vida y la muerte de esta simulación.

## Autor
- Sebastián Benavides

# Clase Enfermedad
La Clase enfermedad tiene como atributos: comunidad, primer infectado, infección probable y promedio de pasos. Los cuales tienen su respectivo getter y setter para utilizar un lenguaje de programación mas ordenado y amplio para la comunicación entre clases, cabe destacar que los atributos están creados de forma privada, excepto comunidad que es llamado desde el archivo main donde es creado por primera vez. ademas designa al primer infectado de forma aleatoria, este es el único que no podrá caer grave sin las vacunas.

Como podemos ver en la imagen 1, aquí se crea el primer infectado.
dato importare: este ciudadano como es el primer infectado, no pasa por las probabilidades de pasar a grave,
ya que si sucediera esto, habría un problema con la simulación y no se podría parecer a la realidad.
![Image text](https://gitlab.com/dj.bannes/programacion-avanzada/-/raw/main/imagenes/Clase%20enfermedad-%20primer%20infectado.png)

imagen 1.
# Clase  Vacuna
La Clase  Vacuna cuenta con un solo atributo que es el nombre, el cual es  asignado desde una función creada en  la Clase Comunidad.

En la imagen 2 muestra como es creada la clase vacuna.
Función creada en la clase comunidad para luego asignar las vacunas a una lista con su respectivo nombre y su identificador único de objeto.

![Image text](https://gitlab.com/dj.bannes/programacion-avanzada/-/raw/main/imagenes/Captura%20de%20pantalla%20de%202022-07-13%2000-40-53.png)

imagen 2.

# Clase Ciudadano
La Clase Ciudadano consta con varios atributos, que hacen referencia a como la enfermedad infecciosa le puede afectar o no, cabe destacar que la clase ciudadano es creada en un while en la clase Comunidad y asignado un identificador para hacer seguimiento del ciudadano, primero se crean todos los ciudadanos y luego se elige uno al azar para ser infectado.

Según la imagen 3, se puede ver que así están definidos todos los atributos utilizados por el ciudadano, para poder darnos detalles mas específicos de como él se podría comportar ante una enfermedad infecciosa.

![Imagen text](https://gitlab.com/dj.bannes/programacion-avanzada/-/raw/main/imagenes/clase%20ciudadano-atributos.png)

imagen 3.
# Clase Comunidad
Esta es una de las Clases mas complejas por la cantidad de funciones que ocurren en ella, contiene, atributos de tipo str e int, como también atributos de tipo lista que hacen referencia a las características que puede tener una comunidad en un ambiente mas realista, con números de habitantes, cantidad de gente infectada, cantidad de gente fallecida entre otras.

La imagen 4, muestra una de las complejidades de este proyecto  que fue diseñado en la clase comunidad, la cual fue la creación de las enfermedades a partir  de posibilidades  que fueron 65% para afecciones, un 25% para  enfermedades y por ende lo que resta de estas posibilidades fueron asignadas a que los ciudadanos no tuvieran ninguna de las dos.
Para el calculo se transformo el porcentaje a números enteros:
n = la cantidad de habitantes 
65% = 65*n/100,
25% =25* n /100,
10% = 10*n/100

![Imagen text](https://gitlab.com/dj.bannes/programacion-avanzada/-/raw/main/imagenes/clase%20comunidad%20-%20designar%20enfermedades.png)

imagen 4.

# Clase Simulador
La Clase Simulador hace referencia a la unión de todas las clases antes descritas , para la interacción entre ellas. En esta clase se podrán encontrar las posibilidades de que ocurran ciertos episodios.

 Según la imagen 5, aquí se pueden ver 2 funciones, la primera es la probabilidad con vacuna la cual ingresa  con la condición de que vacuna sea distinto de None, lo que significa  que si  tiene una vacuna ingresara a un if anidado, el cual indica que si es  la vacuna 1  y si tiene las 2 dosis podrá entrar a una condición de probabilidad de no pasar  a las posibilidades de estar grave, cabe destacar que la posibilidad de no estar grave es baja y no supera el 25%.

La función que se encuentra debajo se llama gravedad de la persona, hace referencia a las posibilidades que puede tener para pasar a grave, en ellas se incluye la edad. las personas sin afecciones ni enfermedades también ingresan a estas posibilidades pero tienen un 60% menos de pasar a graves. En los else se encuentra la función que describe las posibilidades de cada tipo de enfermedad y afecciones respectivamente.

![Imagen text](https://gitlab.com/dj.bannes/programacion-avanzada/-/raw/main/imagenes/simulador%20vacuna%20-%20probabilidad%20y%20gravedad%20de%20la%20persona.png)

imagen 5.

![Imagen text](https://gitlab.com/dj.bannes/programacion-avanzada/-/raw/main/imagenes/probabilidad%20de%20la%20enfermedad%20y%20afeccion.png)

imagen 6.(función que describe las posibilidades de cada tipo de enfermedad y afecciones respectivamente de pasar a graves.)

# Main
En este documento  ocurrirá la reproducción de toda la simulación y con la necesidad de estar todas las clases en un mismo documento,  desde aquí se podrán modificar algunos datos, como la cantidad de ciudadanos para la simulación y algunas probabilidades como la de conexión física y probabilidad de infección de la enfermedad.

![Imagen text](https://gitlab.com/dj.bannes/programacion-avanzada/-/raw/main/imagenes/main%20main%20main.png)

imagen 7.(documento .py donde se ingresan los datos manualmente para poder comenzar la simulacion, la funcion pasos comienza con 30 pasos, una poblacion de 1000 personas y 8 en su lista de contactos estrechos)

![Imagen text](https://gitlab.com/dj.bannes/programacion-avanzada/-/raw/main/imagenes/terminal%20principio.png)

imagen 8.(inicio de la simulacion)

![Imagen text](https://gitlab.com/dj.bannes/programacion-avanzada/-/raw/main/imagenes/Captura%20de%20pantalla%20de%202022-07-13%2001-41-57.png)
imagen 9.(fin de la simulacion, con 30 pasos)
