
class Ciudadano ():
    def __init__(self,_id):
        self.__comunidad = None
        #Para identificar a la comunidad que pertenece el ciudadano 
        self.__id = _id
        #la identificacion del ciudadano 
        self.__amigos = []#grupo de amigos(contactos posibles) 
        self.__estado = True 
        # si es verdadero, indica que el ciudadano esta sano o muerto
        self.__contador = 0 # contador para pasar a estar recuperado 
        self.__enfermedad = None #la enfermedad que tiene el ciudadano
        self.__afeccion = None # la afeccion que tiene el ciudadano 
        self.__edad = None
        self.__estado_de_gravedad = False #entonces no esta grave 
        self.__vacuna = None
        self.__contador_vacuna = None
        self.__muerto = False # false, esta vivo

    #get__comunidad
    def get__comunidad(self):
        return self.__comunidad
    #set__comunidad
    def set__comunidad(self,a):
        self.__comunidad = a

    #get__id
    def get__id(self):
        return self.__id
    #set__id
    def set__id(self,b):
        self.__id = b

    #get__amigos
    def get__amigos(self):
        return self.__amigos
    #set__amigos
    def set__amigos(self,c):
        self.__amigos.append(c)
    #get__estado
    def get__estado(self):
        return self.__estado
    #set__estado
    def set__estado(self,d):
        self.__estado = d
    #get__contador
    def get__contador(self):
        return self.__contador
    #set__contador
    def set__contador(self,e):
        self.__contador = e

    #get__enfermedad
    def get__enfermedad(self):
        return self.__enfermedad
    #set__enfermedad
    def set__enfermedad(self,f):
        self.__enfermedad = f
    #get__afeccion
    def get__afeccion(self):
        return self.__afeccion 
    #set__afeccion  
    def set__afeccion(self,g):
        self.__afeccion = g
    #get__edad
    def get__edad(self):
        return self.__edad
    #set__edad
    def set__edad(self,h):
        self.__edad = h
    #get__estado_de_gravedad
    def get__estado_de_gravedad(self):
        return self.__estado_de_gravedad
    #set__estado_de_gravedad
    def set__estado_de_gravedad(self,z):
        self.__estado_de_gravedad = z
    #get__vacuna
    def get__vacuna(self):
        return self.__vacuna
    #set__vacuna
    def set__vacuna(self,w):
        self.__vacuna = w
    #get__contador_vacuna
    def get__contador_vacuna(self):
        return self.__contador_vacuna
    #set__contador_vacuna
    def set__contador_vacuna(self,x):
        self.__contador_vacuna = x
    #get__muerto
    def get__muerto(self):
        return self.__muerto
    #set__muerto
    def set__muerto(self,y):
        self.__muerto = y
        