from ciudadano import Ciudadano
from vacuna import Vacuna
import random

class Comunidad():
    
    def __init__(self,nombre_comunidad,numero_ciudadanos_ciudad,
                 Numero_de_amigos, probabilidad_conexion_fisica):
        self.__nombre_comunidad = nombre_comunidad
        self.__num_ciudadanos = numero_ciudadanos_ciudad
        # el numero de ciudadanos en la comnunidad
        self.__probabilidad_conexion_fisica = probabilidad_conexion_fisica
        #probabilidad que haya conexion fisica
        self.__Numero_de_amigos = Numero_de_amigos # cantidad de amigos
        self.__Lista_ciudad_completa = []#control para agregar enfermedades
        self.__Lista_ciudad_sana =[] 
        self.__Lista_ciudad_enferma = []
        self.__Lista_ciudad_recuperada = []
        self.__Lista_ciudad_gravedad = []
        self.__Lista_ciudad_fallecidos = []
        '''lista de las posibles enfermedades y lista de posibles afecciones
        que puede tener nuestro ciudadano'''
        self.__Lista_enfermedad_ciudadano = ["asma","enfermedad cerebro vascular",
        "fibrosis Quistica","Hipertension"]
        self.__Lista_afecciones_ciudadano = ["obesidad", "desnutricion"]
        self.__Lista_vacunas =[]
        print("Total de la poblacion: ",self.__num_ciudadanos)
    #get__nombre_comunidad
    def get__nombre_comunidad(self):
        return self.__nombre_comunidad
    #set__nombre_comunidad
    def set__nombre_comunidad(self,a):
        self.__nombre_comunidad = a

    #get__num_ciudadanos
    def get__num_ciudadanos(self):
        return self.__num_ciudadanos
    #set__num_ciudadanos
    def set__num_ciudadanos(self,b):
        self.__num_ciudadanos = b
    
    #get__Numero_de_amigos
    def get__Numero_de_amigos(self):
        return self.__Numero_de_amigos
    #set__Numero_de_amigos
    def set__Numero_de_amigos(self,c):
        self.__Numero_de_amigos = c
    #get__Lista_ciudad_sana
    def get__Lista_ciudad_sana(self):
        return self.__Lista_ciudad_sana
    #set__Lista_ciudad_sana
    def set__Lista_ciudad_sana(self,d):
        self.__Lista_ciudad_sana.append(d)
    #get__Lista_ciudad_enferma
    def get__Lista_ciudad_enferma(self):
        return self.__Lista_ciudad_enferma
    #set__Lista_ciudad_enferma
    def set__Lista_ciudad_enferma(self,e):
        self.__Lista_ciudad_enferma.append(e)
    
    #get__probabilidad_conexion_fisica
    def get__probabilidad_conexion_fisica(self):
        return self.__probabilidad_conexion_fisica
    #set__probabilidad_conexion_fisica
    def set__probabilidad_conexion_fisica(self,f):
        self.__probabilidad_conexion_fisica = f
    #get__Lista_ciudad_recuperada
    def get__Lista_ciudad_recuperada(self):
        return self.__Lista_ciudad_recuperada
    #set__Lista_ciudad_recuperada
    def set__Lista_ciudad_recuperada(self,g):
        self.__Lista_ciudad_recuperada.append(g)
    #get__Lista_enfermedad_ciudadano
    def get__Lista_enfermedad_ciudadano(self):
        return self.__Lista_enfermedad_ciudadano
    #set__Lista_enfermedad_ciudadano
    def set__enfermedad(self,h):
        self.__Lista_enfermedad_ciudadano.append(h)
    #get__Lista_afecciones_ciudadano
    def get__Lista_afecciones_ciudadano(self):
        return self.__Lista_afecciones_ciudadano 
    #set__Lista_afecciones_ciudadano  
    def set__Lista_afecciones_ciudadano(self,z):
        self.__Lista_afecciones_ciudadano.append(z)
    #get__Lista_ciudad_gravedad
    def get__Lista_ciudad_gravedad(self):
        return self.__Lista_ciudad_gravedad
    #set__Lista_ciudad_gravedad
    def set__Lista_ciudad_gravedad(self,x):
        self.__Lista_ciudad_gravedad.append(x)
    #get__Lista_ciudad_completa
    def get__Lista_ciudad_completa(self):
        return self.__Lista_ciudad_completa
    #set__Lista_ciudad_completa
    def set__Lista_ciudad_completa(self,y):
        self.__Lista_ciudad_completa.append(y)
    #get__Lista_ciudad_fallecidos
    def get__Lista_ciudad_fallecidos(self):
        return self.__Lista_ciudad_fallecidos
    #set__Lista_ciudad_fallecidos
    def set__Lista_ciudad_fallecidos(self,w):
        self.__Lista_ciudad_fallecidos.append(w)
    #get__Lista_vacunas
    def get__Lista_vacunas(self):
        return self.__Lista_vacunas
    #set__Lista_vacunas
    def set__Lista_vacunas(self,s):
        self.__Lista_vacunas.append(s)

    
    def crear_ciudadanos(self):
        i = 0
        entregar_id = 0 
        promedio_edad = 0
        while i < self.__num_ciudadanos:

            ciudadanoX = Ciudadano(entregar_id)
            ciudadanoX.set__comunidad(self.get__nombre_comunidad())
            #se entrega el nombre de la comunidad al ciudano
            ciudadanoX.set__edad(random.randint(0,100))
            promedio_edad += ciudadanoX.get__edad()
            #se entrega la edad al ciudadano

            self.__Lista_ciudad_completa.append(ciudadanoX)
            #Lista ciudad completa para asignar enfermedades 

            self.set__Lista_ciudad_sana(ciudadanoX)
            #se envia el ciudadado a la lista de ciudad sana 
                        
            i += 1  
            entregar_id += 1#id del ciudadano
        print("promedio edad: ",promedio_edad/self.__num_ciudadanos)
    def lista_de_amigos(self):
        n = self.__Numero_de_amigos
        #variable corta
        s = len(self.__Lista_ciudad_sana)
        #largo de la lista de ciudadanos 
        for i in self.__Lista_ciudad_sana:
            # recorre la lista de personas sanas 
            j = random.randint( 0, s - n )  
            # numero aleatorio, define el inicio del id de los amigos
            for l in range(0,n):
                vk = j+l 
                # numero aleatorio + el rango entre 0  y n-1
                if vk == i.get__id(): 
                    #si no es == se agrega con normalidad vk
                    # si el rango entre 0 y n-1  es igual al id ingresa al if
                    if vk <= (s-n)-1 :
                    # s-n = ( la lista de ciudadanos - la cantidad de amigos)
                    # (-1 no salirse)
                    #si ingresa se le suma n
                        vk += n
                    elif vk >= n:
                    # ingresa solo cuando es mayor que la condicion anterior
                        vk -= n
                        #si ingresa se le resta n 
                    else : #control para casos extremos(casos limitados)
                        if j+n-1 < s-1:
                            '''cuando la cantidad de amigos es muy cercana 
                            a la cantidad  de ciudadanos totales, entonces el
                            id agregado sera el ultimo o el primero '''
                            vk = s-1
                        else:
                            vk = 0

                i.set__amigos(vk)#se agrega el amigo

    def designar_enfermedad(self):
        n = self.__num_ciudadanos
        Lista_del_65 = []
        Lista_del_25 = []
        Lista_del_10 = [] 
        random.shuffle(self.__Lista_ciudad_completa)
        x = int(65*n /100)
        y = int(25*n /100)
        for i in self.__Lista_ciudad_completa:

            if len(Lista_del_65) < x:
                Lista_del_65.append(i)
                i.set__afeccion(random.choice(self.get__Lista_afecciones_ciudadano()))
            elif len(Lista_del_25) < y:
                Lista_del_25.append(i)
                i.set__enfermedad(random.choice(self.get__Lista_enfermedad_ciudadano()))
            else:
                Lista_del_10.append(i)
        print("Total de poblacion con una enfermedad: ",len(Lista_del_25))
        print("Total de poblacion con una afeccion: ",len(Lista_del_65))
    #creacion de vacunas 
    def crear_vacunas(self):
        vacuna_1 = Vacuna("AstraZeneca")#se crea el objeto vacuna y su nombre
        vacuna_2 = Vacuna("SPUTNIK V")#se crea el objeto vacuna y su nombre
        vacuna_3 = Vacuna("Pfizer-BioNTech")#el objeto vacuna y su nombre
        self.set__Lista_vacunas(vacuna_1)#se añade a la lista de vacunas
        self.set__Lista_vacunas(vacuna_2)#se añade a la lista de vacunas
        self.set__Lista_vacunas(vacuna_3)#se añade a la lista de vacunas
    #funcion que imprime 
    def impresion_general(self):
       
        print("Total de la poblacion: ", self.__num_ciudadanos,
                "susceptibles: ", len(self.__Lista_ciudad_sana),
                "total de contagios son: ",
                (self.__num_ciudadanos) - (len(self.__Lista_ciudad_sana)),
                "total de recuperados: ",len(self.__Lista_ciudad_recuperada),
                "casos activos: ",(len(self.__Lista_ciudad_enferma)),
                "casos graves: ",(len(self.__Lista_ciudad_gravedad)),
                "casos fallecidos: ",(len(self.__Lista_ciudad_fallecidos)))
        #funcion que calcula el promedio de la ciudad viva 
    def promedio_ciudad_viva(self):
        promedio = 0
        contador = 0
        for i in self.__Lista_ciudad_completa:

            if i.get__muerto()== False:#si estan vivos ingresa al if y suma 1 
                contador += 1
                promedio += i.get__edad()#se le suman las edades 
                #print del promedio con desimales 
        print("promedio edad personas vivas: ", promedio/contador)


        
           
           

        
        
