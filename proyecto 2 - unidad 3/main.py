from comunidad import Comunidad
from enfermedad import Enfermedad
from simulador import Simulador
import random


if __name__ == "__main__": 
    
    comunidadX = Comunidad("talca",1000,8,probabilidad_conexion_fisica = 0.2) 
    simuladorX = Simulador(comunidadX)
    primer_infectado = random.randint(0,len(comunidadX.get__Lista_ciudad_sana() )-1)
    # se seleciona el primer infectado 
    enfermedadX = Enfermedad(comunidadX, primer_infectado,
                             infeccion_probable = 0.8, promedio_pasos = 14)
    
    simuladorX.pasos(30,enfermedadX,comienzo_de_vacunacion = 10)


