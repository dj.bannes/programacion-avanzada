from random import random, shuffle
# libreria random  que entrega numeros al azar desde 0 a 1


class Simulador():
    def __init__(self,comunidad):#constructor
               
        self.CiudadX = comunidad
        #comunidad entregada desde el main 
    
        self.CiudadX.crear_ciudadanos()
        #se crean los ciudadanos
        self.CiudadX.lista_de_amigos()
        #crea la lista de amigos
        self.CiudadX.designar_enfermedad()
        #designa una enfermedad o afeccion    
    '''funcion que define la probabilidad de que una persona sea considerada 
    grave despues de vacunarse'''
    def probabilidad_con_vacuna(self,ciudadano):
        if ciudadano.get__vacuna()!= None:
            if ciudadano.get__vacuna().get__nombre_vacuna() == "AstraZeneca" and ciudadano.get__contador_vacuna() == 2:
                if random() > 0.25:#primera vacuna 
                    self.gravedad_de_la_persona(ciudadano)
        #si no tiene vacuna ingresa a probabilidad de gravedad 
        elif ciudadano.get__vacuna() == None :
            self.gravedad_de_la_persona(ciudadano)
        '''funcion que define la gravedad de las personas resiviendo el objeto 
            persona infectada'''
    def gravedad_de_la_persona(self,ciudadano):
        if ciudadano.get__edad() <= 5 or ciudadano.get__edad() >= 71:
             #personas de gravedad por su edad 
            if random() < 0.5:#entre esta edad tienen un 50% de posibilidad
                ciudadano.set__estado_de_gravedad(True)
            else:
                #funcion que verifica la gravedad de la afeccion
                self.probabilidad_de_la_enfermedad_y_afeccion(ciudadano)
        else:
                    
            if ciudadano.get__enfermedad() == None and ciudadano.get__afeccion() == None:

                if random() < 0.4:#persona sin enfermedad ni afeccion
                    ciudadano.set__estado_de_gravedad(True) 
            else:#funcion que verifica la gravedad de la afeccion
                self.probabilidad_de_la_enfermedad_y_afeccion(ciudadano)
    #funcion con las probabilidades de cada tipo de enfermedad y afecciones
    def probabilidad_de_la_enfermedad_y_afeccion(self,persona):

        #enfermedades
        if persona.get__enfermedad != None:
            if persona.get__enfermedad == "asma":
                if random()< 0.2:#posibilidad no tan grave
                    persona.set__estado_de_gravedad(True)
            elif persona.get__enfermedad == "enfermedad cerebro vascular":
                if random()< 0.6:#posibilidad grave
                    persona.set__estado_de_gravedad(True)
            elif persona.get__enfermedad == "fibrosis Quistica":
                if random()< 0.5:#posibilidad grave
                    persona.set__estado_de_gravedad(True)
            elif persona.get__enfermedad == "Hipertension":
                if random()< 0.3:#posibilidad leve
                    persona.set__estado_de_gravedad(True)
        #afecciones
        if persona.get__afeccion != None:
            if persona.get__afeccion == "obesidad":
                if random()< 0.4:#posibilidad afeccion leve
                    persona.set__estado_de_gravedad(True)
            elif persona.get__afeccion == "desnutricion":
                if random()< 0.6:#posibilidad grave
                    persona.set__estado_de_gravedad(True)
       

    '''buscar_id: toma un identificador de cualquier ciudadano(enviado desde la
    funcion buscar amigosI)
    en busqueda del mismo para poder obtener el objeto de tipo ciudadano
    para luego ser retornado a la funcion grupo de amigos'''
    def buscar_id(self,id_buscado):

        for i in self.CiudadX.get__Lista_ciudad_completa():
            if i.get__id() == id_buscado:
                return i
 
    def grupo_amigosI(self, primer_infectado,enfermedad):
        #funcion grupo amigos del infectado 
        grupo_de_amigosI = primer_infectado.get__amigos()
        #se cambia la variable 
        
        for i in grupo_de_amigosI:
            #for que recorre el grupo de amigos
            #se considera que se juntaron 

            if random() < self.CiudadX.get__probabilidad_conexion_fisica():
                '''random que  lanza numeros desimales aleatorios
                entre 0 y 1/// get__probabilidad conexion fisica se entrega
                desde el main, si ingresa es porque si hubo conexion fisica 
                entre las personas '''
                if random() < enfermedad.get__infeccion_probable():
                    '''get__infeccion problable se ingresa desde el main
                    y si ingresa aqui se considera infectado el ciudadano'''
                    persona = self.buscar_id(i)

                    if persona.get__estado() == True:
                    #como el i es solo un numero que representa el id 
                    #se ingresa a la funcion buscar id que obtendra de 
                    # vuelta un objeto
                        if persona.get__vacuna()!= None :
                            if persona.get__vacuna().get__nombre_vacuna()!= "Pfizer-BioNTech":
                                #vacuna que te hace inmune 
                                persona.set__estado(False)
                                self.probabilidad_con_vacuna(persona)
                        else:
                            persona.set__estado(False)
                            self.probabilidad_con_vacuna(persona)                           
                    #se le cambia el estado a la persona
                

    def pasos(self,pasos_totales,enfermedad,comienzo_de_vacunacion):
        print("vacunas totales: ", len(self.CiudadX.get__Lista_vacunas()) )
      #  self.grupo_amigosI(primer_infectado) 
        Lista_temporal_vacuna_1 = []
        Lista_temporal_vacuna_2 = []
        Lista_temporal_vacuna_3 = []
        xx =int(len(self.CiudadX.get__Lista_ciudad_completa())/2 * 9 / 18)
        #proporcion de vacunas 1
        yy =int(len(self.CiudadX.get__Lista_ciudad_completa())/2 * 6 / 18)
        #proporcion de vacunas 3
        zz =int(len(self.CiudadX.get__Lista_ciudad_completa())/2 * 3 / 18)
        #proporcion de vacunas 3
        for i in range(0,pasos_totales):
            print("dia :", i+1 )
            if i+1 == comienzo_de_vacunacion:
                print("vacunas totales: ", xx+yy+zz)
                self.CiudadX.crear_vacunas()
                #si se crean las vacunas ingresa a este if
            if len(self.CiudadX.get__Lista_vacunas()) == 3:
                #shufle que desordena la lista entregada
                shuffle(self.CiudadX.get__Lista_ciudad_completa())
                #for que recorre la lista de ciudadanos completa
                for y in self.CiudadX.get__Lista_ciudad_completa():
                
                    #se revuelve la lista ciudad completa 
                    if y.get__muerto()!= True and y.get__vacuna() == None:
                        '''en esta parte se recorre la lista de saludables para 
                        poder entregarles una vacuna, el orden a vacunar es con 
                        personas aleatoreas, ya que la lista al ingresar se
                        desordeno. Las vacunas seran dadas por cantidad 
                        en orden decreciente
                        '''    
                        if y in self.CiudadX.get__Lista_ciudad_sana():
                            if len(Lista_temporal_vacuna_1)< xx:
                                Lista_temporal_vacuna_1.append(y)
                                y.set__vacuna(self.CiudadX.get__Lista_vacunas()[0])
                            elif len(Lista_temporal_vacuna_2) < yy :
                                Lista_temporal_vacuna_2.append(y)
                                y.set__vacuna(self.CiudadX.get__Lista_vacunas()[1])
                            elif len(Lista_temporal_vacuna_3) < zz :
                                Lista_temporal_vacuna_3.append(y)
                                y.set__vacuna(self.CiudadX.get__Lista_vacunas()[2])
                                y.set__contador_vacuna(1) 
                if i >= comienzo_de_vacunacion+3:
                    #el tercer paso luego de dar comienzo a la vacunacion
                    ''' se ingresa a estas condiciones para la segunda vacunacion
                    a partir del registro de las listas temporales de vacunacion'''
                    for h in Lista_temporal_vacuna_1:
                        if h.get__muerto()!= True:
                            if h in self.CiudadX.get__Lista_ciudad_sana():
                                g.set__contador_vacuna(2)
                if i >= comienzo_de_vacunacion+6:
                    '''despues de la primera dosis y luego de 6 pasos se da comienzo
                    a la segunda dosis de la vacuna 2 '''
                    for g in Lista_temporal_vacuna_2:
                        if g.get__muerto()!= True:
                            if g in self.CiudadX.get__Lista_ciudad_sana():
                                g.set__contador_vacuna(2)               

            for j in self.CiudadX.get__Lista_ciudad_enferma():
                self.grupo_amigosI(j,enfermedad)
            Lista_temporal_enfermos=[]#se actualiza por cada enfermo
            Lista_temporal_gravedad =[]#se actualiza por cada grave
            for k in self.CiudadX.get__Lista_ciudad_sana():
                #recorre la ciudad sana 
                if k.get__estado() == False:
                    if k.get__estado_de_gravedad() == False:#false = no grave
                        #busca a la gente enferma
                        Lista_temporal_enfermos.append(k)
                        #agrega la gente a la lista temporal 
                        self.CiudadX.set__Lista_ciudad_enferma(k)
                        # agrega a la gente enferma a la lista de  enfermos
                    else:
                        Lista_temporal_gravedad.append(k)
                        self.CiudadX.set__Lista_ciudad_gravedad(k)
                        #se agrega a la lista de gravedad 
            for l in Lista_temporal_enfermos:
                #recorre la lista temporal para que no haya 
                # conflicto en el for anterior
                self.CiudadX.get__Lista_ciudad_sana().remove(l)
                #se remueve de la lista de sanos a travez del objeto en 
                #la lista temporal
            for g in Lista_temporal_gravedad:
                #se revisa la lista temporal 
                self.CiudadX.get__Lista_ciudad_sana().remove(g)
                #se eliminan de la lista de ciudad sana
            Lista_temporal_recuperados=[]#se actualiza por cada recuperado
            for m in self.CiudadX.get__Lista_ciudad_enferma():
                '''for para contador  que recorre la lista de enfermos'''
                m.set__contador(m.get__contador()+1)
                #se le entrega el mismo valor que ya tenia y se le suma 1
                if m.get__contador() >= enfermedad.get__promedio_pasos():
                    #se recupera en el dia  >= al promedio de pasos
                    Lista_temporal_recuperados.append(m)
                    #se agrega el ciudadano a la lista temporar de recuperados 
                    self.CiudadX.set__Lista_ciudad_recuperada(m)
                    #append que agrega m a la lista de recuperados
            for n in Lista_temporal_recuperados:
                #for para recorrer la lista temporal y obtener al objeto 
                #recuperado
                self.CiudadX.get__Lista_ciudad_enferma().remove(n)
                #luego de obtenerlo lo remueve de la lista de enfermos 
            Lista_temporal_recuperados_de_gravedad = []#lista temporal 
            Lista_temporal_ciudad_fallecidos =[]#lista temporal de fallecidos
            for o in self.CiudadX.get__Lista_ciudad_gravedad():
                '''for para contador  que recorre la lista de gravedad'''
                o.set__contador(o.get__contador()+1)
                #se le entrega el mismo valor que ya tenia y se le suma 1
                if o.get__contador() >= enfermedad.get__promedio_pasos():
                    #se recupera en el dia  >= al promedio de pasos
                    Lista_temporal_recuperados_de_gravedad.append(o)
                    #se agrega el ciudadano a la lista temporar de recuperados 
                    self.CiudadX.set__Lista_ciudad_recuperada(o)
                    #append que agrega m a la lista de recuperados
                    '''probabilidad de que se mueran: despues  de la mitad
                     de los pasos, la gente tiene una posibilidad de morirse'''
                elif o.get__contador() >= enfermedad.get__promedio_pasos()/2:
                    if random() < 0.3:#se muere
                        Lista_temporal_ciudad_fallecidos.append(o)
                        self.CiudadX.set__Lista_ciudad_fallecidos(o)
                        o.set__muerto(True)
                    

            for p in Lista_temporal_recuperados_de_gravedad:
                #for para recorrer la lista temporal y obtener al objeto 
                #recuperado
                self.CiudadX.get__Lista_ciudad_gravedad().remove(p)
                #luego de obtenerlo lo remueve de la lista de gravedad 
            for q in Lista_temporal_ciudad_fallecidos:
                #for para remover fallecidos 
                self.CiudadX.get__Lista_ciudad_gravedad().remove(q)
                #remueve los fallecidos de la lista de gravedad
            self.CiudadX.impresion_general() #funcion impresora
        self.CiudadX.promedio_ciudad_viva()#promedio edad ciudad viva
        print("total vacunados, vacuna 1: ",len(Lista_temporal_vacuna_1))
        print("total vacunados, vacuna 2: ",len(Lista_temporal_vacuna_2))
        print("total vacunados, vacuna 3: ",len(Lista_temporal_vacuna_3))
